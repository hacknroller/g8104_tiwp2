package es.g8104.wallapop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class G8104CatalogWallapopMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(G8104CatalogWallapopMicroserviceApplication.class, args);
	}
}