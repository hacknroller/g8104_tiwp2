package es.g8104.wallapop.catalog;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import es.g8104.wallapop.data.IdProducto;
import es.g8104.wallapop.data.Owner;
import es.g8104.wallapop.data.Producto;
import es.g8104.wallapop.data.ProductoMongoRepository;
import es.g8104.wallapop.data.Result;
import es.g8104.wallapop.data.Search;

@RestController
//The following annotation is necessary to allow 
//different domains to be able to use this microservice
@CrossOrigin
public class CatalogController {
	@Autowired
	ProductoMongoRepository repository;
	
	//CREATE
	@RequestMapping(value = "/createProduct", method = RequestMethod.POST)
	public Producto createProduct(@RequestBody Producto newProduct) {		
		Producto savedProdut = repository.save(newProduct);
		
		return savedProdut;
	}
	
	//READ
	@RequestMapping(value = "/readMostRecenProducts", method = RequestMethod.GET)
	public List<Producto> readProducts() {
		List<Producto> products = repository.findTop4ByOrderByIdDesc();
		return products;
	}
	
	@RequestMapping(value = "/readByEmailOwner", method = RequestMethod.POST)
	public List<Producto> readProductsByEmaiOwner(@RequestBody Owner owner) {
		List<Producto> products = repository.findByEmailOwner(owner.getEmailOwner());
		return products;
	}
	
	@RequestMapping(value = "/readById", method = RequestMethod.POST)
	public Producto readProductById(@RequestBody IdProducto idProducto) {
		Producto product = repository.findOne(idProducto.getId());
		return product;
	}
	
	@RequestMapping(value = "/readByTitleAndDescription", method = RequestMethod.POST)
	public List<Producto> readByTitleAndDescription(@RequestBody Search search) {
		List<Producto> result = repository.findByTitleIgnoreCaseContainingOrDescriptionIgnoreCaseContaining(search.getTitle(), search.getDescription());
		return result;
	}
	
	@RequestMapping(value = "/advancedSearchProduct", method = RequestMethod.POST)
	public List<Producto> advancedSearchProduct(@RequestBody Search search) {
		List<Producto> result = repository.findByTitleIgnoreCaseContainingAndDescriptionIgnoreCaseContainingAndCategoryIgnoreCaseContainingAndEmailOwnerIgnoreCaseContaining(search.getTitle(), search.getDescription(), search.getCategory(), search.getUser());
		return result;
	}
	
	@RequestMapping(value = "/readAllProducts", method = RequestMethod.GET)
	public List<Producto> readAllProducts() {
		List<Producto> result = repository.findAll();
		return result;
	}
	
	//UPDATE
	@RequestMapping(value = "/modifyProduct", method = RequestMethod.POST)
	public Result modifyClient(@RequestBody Producto updatedProduct) {
		Result result = new Result();

		//If the product exists
		if(repository.exists(updatedProduct.getId())){
			repository.save(updatedProduct);
			result.setResult(true);
		}
		else result.setResult(false);
		
		return result;
	}
	
	//DELETE
	@RequestMapping(value = "/deleteProduct", method = RequestMethod.POST)
	public Result deleteProduct(@RequestBody Producto deleteProduct) {
		Result result = new Result();

		//If the product exists
		if(repository.exists(deleteProduct.getId())){
			repository.delete(deleteProduct);
			result.setResult(true);
		}
		else result.setResult(false);
		
		return result;
	}
	
	@RequestMapping(value = "/removeByUser", method = RequestMethod.POST)
	public void removeByUser(@RequestBody Producto product) {
		repository.removeByEmailOwner(product.getEmailOwner());
	}
}