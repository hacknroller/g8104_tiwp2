package es.g8104.wallapop.data;

public class IdProducto {
	private String id;
	
	public IdProducto(){}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}	
}