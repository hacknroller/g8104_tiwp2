package es.g8104.wallapop.data;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ProductoMongoRepository extends CrudRepository<Producto, String> {
	List<Producto> findByEmailOwner(String emailOwner);
	List<Producto> findTop4ByOrderByIdDesc();
	List<Producto> findByTitleIgnoreCaseContainingOrDescriptionIgnoreCaseContaining(String title, String description);
	List<Producto> findByTitleIgnoreCaseContainingAndDescriptionIgnoreCaseContainingAndCategoryIgnoreCaseContainingAndEmailOwnerIgnoreCaseContaining(String title, String description, String category, String emailOwner);
	List<Producto> findAll();
	List<Producto> removeByEmailOwner(String emailOwner);
}