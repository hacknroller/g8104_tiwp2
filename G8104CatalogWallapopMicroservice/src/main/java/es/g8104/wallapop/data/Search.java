package es.g8104.wallapop.data;

import java.io.Serializable;

public class Search implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String title;
	
	private String description;
	
	private String category;
	
	private String user;
	
	public Search(){}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
}
