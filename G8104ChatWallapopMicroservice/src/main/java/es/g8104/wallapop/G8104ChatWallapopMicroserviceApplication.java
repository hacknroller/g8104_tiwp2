package es.g8104.wallapop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class G8104ChatWallapopMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(G8104ChatWallapopMicroserviceApplication.class, args);
	}
}
