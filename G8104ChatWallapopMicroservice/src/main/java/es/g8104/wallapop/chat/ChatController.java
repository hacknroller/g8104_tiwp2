package es.g8104.wallapop.chat;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import es.g8104.wallapop.data.Chat;
import es.g8104.wallapop.data.ChatMessage;
import es.g8104.wallapop.data.ChatMessageMongoRepository;
import es.g8104.wallapop.data.Result;
import es.g8104.wallapop.data.Usuario;

@RestController
//The following annotation is necessary to allow 
//different domains to be able to use this microservice
@CrossOrigin
public class ChatController {
	@Autowired
	ChatMessageMongoRepository repository;
	
	//CREATE
	@RequestMapping(value = "/createMessage", method = RequestMethod.POST)
	public Result createMessage(@RequestBody ChatMessage message) {
		repository.save(message);
		
		Result result = new Result();
		result.setResult(true);
		
		return result;
	}
	
	//READ
	@RequestMapping(value = "/readChats", method = RequestMethod.POST)
	public List<Chat> readChats(@RequestBody Usuario user) {
		List<ChatMessage> messagesList = repository.findBySellerOrBuyer(user.getEmail(), user.getEmail());
		List<Chat> result = new ArrayList<Chat>();
		
		for(ChatMessage current : messagesList){
			Chat newChat = new Chat();
			newChat.setSeller(current.getSeller());
			newChat.setBuyer(current.getBuyer());
			newChat.setProduct(current.getProduct());
			
			if(!result.contains(newChat)) result.add(newChat);
		}

		return result;
	}
	
	@RequestMapping(value = "/readMessages", method = RequestMethod.POST)
	public List<ChatMessage> readMessages(@RequestBody Chat chat) {
		List<ChatMessage> result = repository.findBySellerAndBuyerAndProduct(chat.getSeller(), chat.getBuyer(), chat.getProduct());
		
		return result;
	}
	
	//DELETE
	@RequestMapping(value = "/removeByProduct", method = RequestMethod.POST)
	public void removeByProduct(@RequestBody Chat chat) {
		repository.removeByProduct(chat.getProduct());
	}
	

	@RequestMapping(value = "/removeBySellerOrBuyer", method = RequestMethod.POST)
	public void removeBySellerOrBuyer(@RequestBody Chat chat) {
		repository.removeByBuyer(chat.getBuyer());
		repository.removeBySeller(chat.getSeller());
	}
}