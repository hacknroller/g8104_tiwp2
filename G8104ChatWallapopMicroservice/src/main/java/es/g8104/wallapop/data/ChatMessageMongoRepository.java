package es.g8104.wallapop.data;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ChatMessageMongoRepository extends CrudRepository<ChatMessage, String> {
	List<ChatMessage> findBySellerAndBuyerAndProduct(String seller, String buyer, String product);
	List<ChatMessage> findBySellerOrBuyer(String seller, String buyer);
	List<ChatMessage> removeByProduct(String product);
	List<ChatMessage> removeBySeller(String seller);
	List<ChatMessage> removeByBuyer(String buyer);
}