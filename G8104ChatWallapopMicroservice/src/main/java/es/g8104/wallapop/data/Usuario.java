package es.g8104.wallapop.data;

public class Usuario {
	private String email;

	private String city;

	private String firstsurname;

	private String name;

	private String password;

	private String secondsurname;
	
	public Usuario() {}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getFirstsurname() {
		return this.firstsurname;
	}

	public void setFirstsurname(String firstsurname) {
		this.firstsurname = firstsurname;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecondsurname() {
		return this.secondsurname;
	}

	public void setSecondsurname(String secondsurname) {
		this.secondsurname = secondsurname;
	}
}