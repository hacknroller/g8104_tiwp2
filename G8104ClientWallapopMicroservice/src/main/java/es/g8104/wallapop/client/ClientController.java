package es.g8104.wallapop.client;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import es.g8104.wallapop.data.Result;
import es.g8104.wallapop.data.Usuario;
import es.g8104.wallapop.data.UsuarioMongoRepository;

@RestController
//The following annotation is necessary to allow 
//different domains to be able to use this microservice
@CrossOrigin
public class ClientController {
	@Autowired
	UsuarioMongoRepository repository;

	//CREATE
	@RequestMapping(value = "/createClient", method = RequestMethod.POST)
	public Result createClient(@RequestBody Usuario newUser) {
		Result result = new Result();
		
		//If new user does not exist
		if(!repository.exists(newUser.getEmail())){
			repository.save(newUser);
			result.setResult(true);
		}
		else result.setResult(false);

		return result;
	}
	
	//READ
	@RequestMapping(value = "/loginClient", method = RequestMethod.POST)
	public Usuario loginClient(@RequestBody Usuario credentials) {
		Usuario resultUser = new Usuario();
		Usuario wantedUser = repository.findOne(credentials.getEmail());
		
		//Si existe el usuario
		if(wantedUser != null)
			if(credentials.getPassword().equals(wantedUser.getPassword())) resultUser = wantedUser;
				
		return resultUser;
	}
	
	@RequestMapping(value = "/getClient", method = RequestMethod.POST)
	public Usuario getClient(@RequestBody Usuario credentials) {
		Usuario resultUser = new Usuario();
		Usuario wantedUser = repository.findOne(credentials.getEmail());
		
		//Si existe el usuario
		if(wantedUser != null)
			resultUser = wantedUser;
				
		return resultUser;
	}
	
	@RequestMapping(value = "/loginAdmin", method = RequestMethod.POST)
	public Usuario loginAdmin(@RequestBody Usuario credentials) {
		Usuario resultUser = new Usuario();
		Usuario wantedUser = repository.findOne(credentials.getEmail());
		
		//Si existe el usuario
		if(wantedUser != null)
			if(credentials.getEmail().equals("admin@admin.com"))
				if(credentials.getPassword().equals(wantedUser.getPassword())) resultUser = wantedUser;
				
		return resultUser;
	}
	
	@RequestMapping(value = "/readAllClients", method = RequestMethod.GET)
	public List<Usuario> readAllClients() {
		List<Usuario> result = repository.findAll();

		return result;
	}
	
	//UPDATE
	@RequestMapping(value = "/modifyClient", method = RequestMethod.POST)
	public Result modifyClient(@RequestBody Usuario updatedUser) {
		Result result = new Result();

		//If the user exists
		if(repository.exists(updatedUser.getEmail())){
			repository.save(updatedUser);
			result.setResult(true);
		}
		else result.setResult(false);
		
		return result;
	}
	
	//DELETE
	@RequestMapping(value = "/deleteClient", method = RequestMethod.POST)
	public Result deleteClient(@RequestBody Usuario deleteUser) {
		Result result = new Result();

		//If the user exists
		if(repository.exists(deleteUser.getEmail())){
			repository.delete(deleteUser);
			result.setResult(true);
		}
		else result.setResult(false);
		
		return result;
	}
}