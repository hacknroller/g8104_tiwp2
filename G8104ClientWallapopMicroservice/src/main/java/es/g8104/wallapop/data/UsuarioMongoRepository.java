package es.g8104.wallapop.data;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface UsuarioMongoRepository extends CrudRepository<Usuario, String> {
	List<Usuario> findAll(); 
}