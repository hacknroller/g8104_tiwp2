<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <title>Administracion de productos | Wallapop</title>
	    <link href="css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/font-awesome.min.css" rel="stylesheet">
	    <link href="css/prettyPhoto.css" rel="stylesheet">
	    <link href="css/price-range.css" rel="stylesheet">
	    <link href="css/animate.css" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet">
		<link href="css/responsive.css" rel="stylesheet">
	    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
	    <![endif]-->       
	    <link rel="shortcut icon" href="images/ico/favicon.ico">
	    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	</head><!--/head-->

	<body>
		<header id="header"><!--header-->
			<div class="header-middle"><!--header-middle-->
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<div class="logo pull-left">
								<a href="/G8104adminP2/adminAccountPage"><img src="images/home/logo.png" alt="" /></a>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="shop-menu pull-right">
								<ul class="nav navbar-nav">
								<li><a href="/G8104adminP2/logout" class="active"><i class="fa fa-lock"></i> Logout</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div><!--/header-middle-->
		
			<div class="header-bottom"><!--header-bottom-->
				<div class="container">
					<div class="row">
						<div class="col-sm-9">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div class="mainmenu pull-left">
								<ul class="nav navbar-nav collapse navbar-collapse">
									<li><a href="/G8104adminP2/adminAccountPage">Volver</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div><!--/header-bottom-->
							<h2 class="title text-center">Gestion de productos</h2>
		</header><!--/header-->
		
		<div class="container">
			<div class="row">
					<c:if test="${listOfProducts != null}">
						<div class="wrapper23">
  							<div class="table23">
							    <div class="header23">
									<div class="cell23">Titulo</div>
									<div class="cell23">Categoria</div>
									<div class="cell23">Estado</div>
									<div class="cell23">Precio</div>
									<div class="cell23">Vendedor</div>
									<div class="cell23">Modificar</div>
							    </div>
							    <c:forEach items="${listOfProducts}" begin="0" end="${lastProduct}" var="currentProduct" varStatus="ii">
								    <div class="row23">
										<div class="cell23">${currentProduct.getTitle()}</div>
										<div class="cell23">${currentProduct.getCategory()}</div>
										<div class="cell23">${currentProduct.getState()}</div>
										<div class="cell23">${currentProduct.getPrice()}</div>
										<div class="cell23">${usersProducts.get(ii.index).getEmail()}</div>
										<div class="cell23">
											<form method="POST" action="/G8104adminP2/modifyProductPage">
												<input type="hidden" name="productToModify" value="<c:url value="${currentProduct.getId()}"/>"/>
												<button type="submit">Modificar</button>
											</form>										
										</div>
								    </div>
							    </c:forEach>
    						</div>
    					</div>
					
					</c:if>
					<c:if test="${noProductsYet != null}">
						<p style="color:gray;" class="text-center"><c:out value="${noProductsYet}"/></p>
						<br><br>
					</c:if>	
			</div>
		</div>		

		<footer id="footer"><!--Footer-->
		<h2 class="title text-center"></h2>		
		</footer><!--/Footer-->
		
	    <script src="js/jquery.js"></script>
		<script src="js/price-range.js"></script>
	    <script src="js/jquery.scrollUp.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	    <script src="js/jquery.prettyPhoto.js"></script>
	    <script src="js/main.js"></script>
	</body>
</html>