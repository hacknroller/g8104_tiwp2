<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <title>Modificar usuario | E-Shopper</title>
	    <link href="css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/font-awesome.min.css" rel="stylesheet">
	    <link href="css/prettyPhoto.css" rel="stylesheet">
	    <link href="css/price-range.css" rel="stylesheet">
	    <link href="css/animate.css" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet">
		<link href="css/responsive.css" rel="stylesheet">
	    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
	    <![endif]-->       
	    <link rel="shortcut icon" href="images/ico/favicon.ico">
	    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	</head><!--/head-->

	<body>
		<header id="header"><!--header-->
			<div class="header-middle"><!--header-middle-->
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<div class="logo pull-left">
								<a href="/G8104adminP2/adminAccountPage"><img src="images/home/logo.png" alt="" /></a>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="shop-menu pull-right">
								<ul class="nav navbar-nav">
								<li><a href="/G8104adminP2/logout" class="active"><i class="fa fa-lock"></i> Logout</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div><!--/header-middle-->
		
			<div class="header-bottom"><!--header-bottom-->
				<div class="container">
					<div class="row">
						<div class="col-sm-9">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div class="mainmenu pull-left">
								<ul class="nav navbar-nav collapse navbar-collapse">
									<form method="POST" action="/G8104adminP2/adminUsersPage">								
										<button type="submit" class="btn btn-default add-to-cart">Volver</button>
									</form>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div><!--/header-bottom-->
		</header><!--/header-->
	
	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Datos del usuario</h2>
						<jsp:useBean id="user" class="g8104.admin.data.Usuario" scope="request" />
						<p><b>Nombre:</b><br><jsp:getProperty property="name" name="user"/></p>
						<br>
						<p><b>Primer apellido:</b><br><jsp:getProperty property="firstsurname" name="user"/></p>
						<br>
						<p><b>Segundo apellido:</b><br><jsp:getProperty property="secondsurname" name="user"/></p>
						<br>
						<p><b>Email:</b><br><jsp:getProperty property="email" name="user"/></p>
						<br>
						<p><b>Ciudad:</b><br><jsp:getProperty property="city" name="user"/></p>
					</div><!--/sign up form-->
				</div>
				
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Modificar datos</h2>
						<form method="POST" action="/G8104adminP2/modifyUser">
							<input type="text" name="nameChange" placeholder="Nombre"/>
							<input type="text" name="firstsurnameChange" placeholder="Primer apellido"/>
							<input type="text" name="secondsurnameChange" placeholder="Segundo apellido"/>
							<input type="password" name="oldPasswordChange" placeholder="Antiguo Password"/>
							<input type="password" name="newPasswordChange" placeholder="Nuevo Password"/>
							<input type="text" name="ciudad" placeholder="Ciudad"/>
							<button type="submit" class="btn btn-default">Editar</button>
							<input type="hidden" name="userToModify" value="<c:url value="${user.getEmail()}"/>"/>
						</form>
						<c:if test="${changeFailMessage != null}">
							<br>
							<p style="color:red;"><c:out value="${changeFailMessage}"/></p>
						</c:if>
					</div><!--/sign up form-->
				</div>

				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Dar de baja</h2>
						<form method="POST" action="/G8104adminP2/removeUser">
							<input type="password" name="passwordDelete" placeholder="Password"/>
							<button type="submit" class="btn btn-default">Eliminar cuenta</button>
							<input type="hidden" name="userToModify" value="<c:url value="${user.getEmail()}"/>"/>
						</form>
						<c:if test="${removeFailMessage != null}">
							<br>
							<p style="color:red;"><c:out value="${removeFailMessage}"/></p>
						</c:if>
					</div><!--/sign up form-->
				</div>
			</div>
			<br><br><br>
		</div>
	</section><!--/form-->

		<footer id="footer"><!--Footer-->
		</footer><!--/Footer-->
		
	    <script src="js/jquery.js"></script>
		<script src="js/price-range.js"></script>
	    <script src="js/jquery.scrollUp.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	    <script src="js/jquery.prettyPhoto.js"></script>
	    <script src="js/main.js"></script>
	</body>
</html>