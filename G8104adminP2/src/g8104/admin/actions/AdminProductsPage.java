package g8104.admin.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import g8104.admin.data.Producto;
import g8104.admin.data.Usuario;
import g8104.admin.microservices.CatalogMicroserviceAdminProxy;
import g8104.admin.microservices.ClientMicroserviceAdminProxy;

public class AdminProductsPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba si esta logueado
		HttpSession session = request.getSession(false);
		if (session == null) return new HomePage().handleRequest(request, response);
		else if(session != null) 
			if(session.getAttribute("user") == null) return new HomePage().handleRequest(request, response);
		
		//Lista de todos los productos
		List<Producto> listOfProducts = new CatalogMicroserviceAdminProxy().findAllProducts();
				
		if(!listOfProducts.isEmpty()){
			request.setAttribute("listOfProducts", listOfProducts);
			request.setAttribute("lastProduct", listOfProducts.size()-1);
			
			//Usuarios asociados a los productos
			List<Usuario> usersProducts = new ArrayList<Usuario>();
			for(Producto current: listOfProducts){
				usersProducts.add(new ClientMicroserviceAdminProxy().findUserById(current.getEmailOwner()));
			}
			request.setAttribute("usersProducts", usersProducts);
		}
		else request.setAttribute("noProductsYet", "No articulos registrados por el momento");
		
		return "adminProducts.jsp";
	}
}