package g8104.admin.actions;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import g8104.admin.data.Usuario;
import g8104.admin.microservices.ClientMicroserviceAdminProxy;

public class AdminUsersPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba si esta logueado
		HttpSession session = request.getSession(false);
		if (session == null) return new HomePage().handleRequest(request, response);
		else if(session != null) 
			if(session.getAttribute("user") == null) return new HomePage().handleRequest(request, response);
				
		List<Usuario> listOfUsers = new ClientMicroserviceAdminProxy().findAllUsers();
		
		if(!listOfUsers.isEmpty()){
			request.setAttribute("listOfUsers", listOfUsers);
			request.setAttribute("lastUser", listOfUsers.size()-1);
		}
		else request.setAttribute("noUsersYet", "No hay usuarios registrados por el momento");
		
		return "adminUsers.jsp";
	}
}