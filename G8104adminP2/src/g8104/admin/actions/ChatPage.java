package g8104.admin.actions;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.admin.data.ChatMessage;
import g8104.admin.microservices.ChatMicroserviceAdminProxy;


public class ChatPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Si el vendedor se consulta a si mismo, no se le permite, se redirige a home
		if(request.getParameter("seller").equals(request.getParameter("buyer"))) return new HomePage().handleRequest(request, response);
	
		//Obtener mensajes del chat y setear los atributos pertinentes
		List<ChatMessage> messages = new ChatMicroserviceAdminProxy().getMessages(request);
		request.setAttribute("messages", messages);
		request.setAttribute("seller",request.getParameter("seller"));
		request.setAttribute("buyer",request.getParameter("buyer"));
		request.setAttribute("product",request.getParameter("product"));
		request.setAttribute("productTitle", request.getParameter("productTitle"));
		
		return "chat.jsp";
	}
}