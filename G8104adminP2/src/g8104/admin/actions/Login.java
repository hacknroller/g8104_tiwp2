package g8104.admin.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.admin.microservices.ClientMicroserviceAdminProxy;


public class Login implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba los datos de login, si son correctos se redirige a home
		if(new ClientMicroserviceAdminProxy().loginAdmin(request)) return new AdminAccountPage().handleRequest(request, response);
		else{
			request.setAttribute("loginFail", "Las credenciales introducidas son incorrectas. IntÚntelo de nuevo.");
			return "index.jsp";
		}
	}
}