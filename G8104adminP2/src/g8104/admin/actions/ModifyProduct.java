package g8104.admin.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import g8104.admin.data.Producto;
import g8104.admin.microservices.CatalogMicroserviceAdminProxy;

public class ModifyProduct implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba si esta logueado
		HttpSession session = request.getSession(false);
		if (session == null) return new HomePage().handleRequest(request, response);
		else if(session != null) 
			if(session.getAttribute("user") == null) return new HomePage().handleRequest(request, response);
				
		Producto product = new CatalogMicroserviceAdminProxy().modifyProduct(request);
		
		if(product != null){
			request.setAttribute("userProductName", request.getParameter("userProductName"));
			request.setAttribute("userProductSurname", request.getParameter("userProductSurname"));
			request.setAttribute("userProductCity", request.getParameter("userProductCity"));
			request.setAttribute("product", product);
			return "productDetails.jsp";
		}
		else
			return new AdminProductsPage().handleRequest(request, response);
	}
}