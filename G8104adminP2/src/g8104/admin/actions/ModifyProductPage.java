package g8104.admin.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import g8104.admin.data.Producto;
import g8104.admin.data.Usuario;
import g8104.admin.microservices.CatalogMicroserviceAdminProxy;
import g8104.admin.microservices.ClientMicroserviceAdminProxy;

public class ModifyProductPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba si esta logueado
		HttpSession session = request.getSession(false);
		if (session == null) return new HomePage().handleRequest(request, response);
		else if(session != null) 
			if(session.getAttribute("user") == null) return new HomePage().handleRequest(request, response);
				
		//Se obtiene el producto a mostrar
		Producto product = new CatalogMicroserviceAdminProxy().findProductById(request);
		request.setAttribute("product", product);
		
		//Y el usuario asociado
		Usuario userProduct = new ClientMicroserviceAdminProxy().findUserById(product.getEmailOwner());
		request.setAttribute("userProductName", userProduct.getName());
		request.setAttribute("userProductSurname", userProduct.getFirstsurname());
		request.setAttribute("userProductCity", userProduct.getCity());		
		
		return "productDetails.jsp";
	}
}