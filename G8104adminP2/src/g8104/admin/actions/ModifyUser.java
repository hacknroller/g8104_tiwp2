package g8104.admin.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import g8104.admin.microservices.ClientMicroserviceAdminProxy;

public class ModifyUser implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba si esta logueado
		HttpSession session = request.getSession(false);
		if (session == null) return new HomePage().handleRequest(request, response);
		else if(session != null) 
			if(session.getAttribute("user") == null) return new HomePage().handleRequest(request, response);		
		
		if(new ClientMicroserviceAdminProxy().modifyUser(request)) return "userDetails.jsp";
		else{
			request.setAttribute("changeFailMessage", "Las password introducidas no coinciden");
			return "userDetails.jsp";
		}
	}
}