package g8104.admin.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import g8104.admin.data.Usuario;
import g8104.admin.microservices.ClientMicroserviceAdminProxy;

public class ModifyUserPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba si esta logueado
		HttpSession session = request.getSession(false);
		if (session == null) return new HomePage().handleRequest(request, response);
		else if(session != null) 
			if(session.getAttribute("user") == null) return new HomePage().handleRequest(request, response);
						
		//Se obtiene el usuario deseado para modificar
		Usuario user = new ClientMicroserviceAdminProxy().findUserByEmail(request);
		
		//Se redirecciona correctamente
		if(user.getEmail() != null){
			request.setAttribute("user", user);
			return "userDetails.jsp";
		}
		else return new AdminUsersPage().handleRequest(request, response);
	}
}