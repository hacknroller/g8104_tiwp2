package g8104.admin.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import g8104.admin.microservices.CatalogMicroserviceAdminProxy;

public class RemoveProduct implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba si esta logueado
		HttpSession session = request.getSession(false);
		if (session == null) return new HomePage().handleRequest(request, response);
		else if(session != null) 
			if(session.getAttribute("user") == null) return new HomePage().handleRequest(request, response);
				
		if(new CatalogMicroserviceAdminProxy().removeProduct(request)) return new AdminProductsPage().handleRequest(request, response);
		else request.setAttribute("removeFailMessage", "No ha sido posible eliminar el producto");
		
		return new ModifyProductPage().handleRequest(request, response);
	}
}