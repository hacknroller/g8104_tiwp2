package g8104.admin.business.image;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import g8104.admin.info.Properties;


public class ImageBusinessDefault implements ImageBusiness {
	@Override
	public String uploadImage(HttpServletRequest request, String newID) throws IOException, ServletException, NamingException{
		//Variables a usar
	    final String DestinationPath = Properties.getImagesDestinationPath();
	    final Part filePart = request.getPart("file");
	    final String fileName = newID + ".jpg";
	    OutputStream out = null;
	    InputStream filecontent = null;
	    
	    //Destino
	    out = new FileOutputStream(new File(DestinationPath + File.separator + fileName));
	    
	    //Lectura y escritura
	    filecontent = filePart.getInputStream();
	    int read = 0;
        final byte[] bytes = new byte[1024];
        while ((read = filecontent.read(bytes)) != -1) {
            out.write(bytes, 0, read);
        }
        
        //Cierre
        if (out != null) 
            out.close();

        if (filecontent != null)
            filecontent.close();
        
        return fileName;
	}
}