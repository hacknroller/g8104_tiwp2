package g8104.admin.data;

import java.io.Serializable;

public class Result implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private boolean result;
	
	public Result(){}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}
}