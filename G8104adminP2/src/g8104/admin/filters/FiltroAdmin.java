package g8104.admin.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter(filterName = "FiltroAdmin", servletNames = "FrontControllerServletAdmin", urlPatterns = "/*")
public class FiltroAdmin implements Filter {
    public FiltroAdmin() {}
	public void destroy() {}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = ((HttpServletRequest)request);
		String url = req.getServletPath();
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		if(url.contains(".jsp")){
			request.getRequestDispatcher("error.jsp").forward(request, response);
		}
				
		chain.doFilter(request, httpResponse);
		return;
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
	}
}