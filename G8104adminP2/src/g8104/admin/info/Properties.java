package g8104.admin.info;

import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;

public class Properties {
	/**
	 * Fichero properties
	 */
	private static final String nombreProperties = "g8104.admin.info.InfoAplicacion";

	/**
	 * Path de destino de las imagenes subidas
	 */
	private static String imagesDestinationPath;
		
	/**
	 * Constructor
	 */
	public Properties() {
		super();
	}
	
	/**
	 * Obtiene el path de destino de imagenes subidas
	 * @return path de destino de las imagenes subidas
	 */
	public static String getImagesDestinationPath() {
		if (imagesDestinationPath == null)
			cagarProperties();

		return imagesDestinationPath;
	}
	
	/**
	 * 
	 * @throws MissingResourceException
	 */
	private static void cagarProperties() throws MissingResourceException {

		PropertyResourceBundle appProperties = null;

		try {

			appProperties = (PropertyResourceBundle) PropertyResourceBundle
					.getBundle(nombreProperties);

			imagesDestinationPath = appProperties.getString("Info.imagesDestinationPath");

		} catch (MissingResourceException e) {

			throw e;
		}

	}
}