package g8104.admin.microservices;

import java.io.IOException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import g8104.admin.data.Chat;
import g8104.admin.data.IdProducto;
import g8104.admin.data.Producto;
import g8104.admin.data.Result;

public class CatalogMicroserviceAdminProxy {
	public List<Producto> findAllProducts(){
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8020").path("/readAllProducts");
		//Consumicion del microservicio
		List<Producto> result=	webResource.request("application/json").accept("application/json").get(new GenericType<List<Producto>>() {});

		return result;
	}
	
	public Producto findProductById(HttpServletRequest request) {
		IdProducto idProducto = new IdProducto();
		idProducto.setId(request.getParameter("productToModify"));
		
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8020").path("/readById");
		//Consumicion del microservicio
		Producto result = webResource.request("application/json").accept("application/json").post(Entity.entity(idProducto,MediaType.APPLICATION_JSON),Producto.class);
		
		return result;
	}
	
	public Producto modifyProduct(HttpServletRequest request) {
		//Se obtiene el producto a modificar
		IdProducto idProducto = new IdProducto();
		idProducto.setId(request.getParameter("productToModify"));
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8020").path("/readById");
		//Consumicion del microservicio
		Producto productChanges = webResource.request("application/json").accept("application/json").post(Entity.entity(idProducto,MediaType.APPLICATION_JSON),Producto.class);
		
		if(!request.getParameter("titleChange").equals("")) productChanges.setTitle(request.getParameter("titleChange"));
		if(request.getParameter("estadoChange") != null) productChanges.setState(request.getParameter("estadoChange"));
		if(request.getParameter("categoriaChange") != null) productChanges.setCategory(request.getParameter("categoriaChange"));
		if(!request.getParameter("descriptionChange").equals("")) productChanges.setDescription(request.getParameter("descriptionChange"));
		if(!request.getParameter("priceChange").equals("")) productChanges.setPrice(request.getParameter("priceChange"));
		
		//Update image
		try {
			if(!request.getPart("file").getSubmittedFileName().equals("")){
				try {
					new g8104.admin.business.image.ImageBusinessDefault().uploadImage(request, productChanges.getId());
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				} catch (ServletException e) {
					e.printStackTrace();
					return null;
				} catch (NamingException e) {
					e.printStackTrace();
					return null;
				}			
			}
		} catch (IOException | ServletException e) {
			e.printStackTrace();
		}
		
		//Modificar el producto
		//Preparacion para consumir microservicio
		Client client2 = ClientBuilder.newClient();
		WebTarget webResource2 = client2.target("http://localhost:8020").path("/modifyProduct");
		//Consumicion del microservicio
		Result result = webResource2.request("application/json").accept("application/json").post(Entity.entity(productChanges,MediaType.APPLICATION_JSON),Result.class);
		
		if(result.isResult()) return productChanges;
		else return null;
	}
	
	public boolean removeProduct(HttpServletRequest request) {
		//Se rellena el producto a modificar (solo interesa el id)
		Producto deleteProduct = new Producto();
		deleteProduct.setId(request.getParameter("productToModify"));

		//Preparacion para consumir microservicio
		Client client2 = ClientBuilder.newClient();
		WebTarget webResource2 = client2.target("http://localhost:8020").path("/deleteProduct");
		//Consumicion del microservicio
		Result result = webResource2.request("application/json").accept("application/json").post(Entity.entity(deleteProduct,MediaType.APPLICATION_JSON),Result.class);

		//Si se completo la operacion con exito
		if(result.isResult()) {
			Chat chat = new Chat();
			chat.setProduct(deleteProduct.getId());
			
			//Preparacion para consumir microservicio
			Client client3 = ClientBuilder.newClient();
			WebTarget webResource3 = client3.target("http://localhost:8030").path("/removeByProduct");
			//Consumicion del microservicio
			webResource3.request("application/json").accept("application/json").post(Entity.entity(chat,MediaType.APPLICATION_JSON));

			return true;
		}
		else return false;
	}
}