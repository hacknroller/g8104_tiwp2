package g8104.admin.microservices;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import g8104.admin.data.Chat;
import g8104.admin.data.Producto;
import g8104.admin.data.Result;
import g8104.admin.data.Usuario;

public class ClientMicroserviceAdminProxy {
	public boolean loginAdmin(HttpServletRequest request){
		//Rellenar usuario credenciales
		Usuario userCredentials = new Usuario();
		userCredentials.setEmail(request.getParameter("email"));
		userCredentials.setPassword(request.getParameter("password"));
		
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8010").path("/loginAdmin");
		//Consumicion del microservicio
		Usuario result=	webResource.request("application/json").accept("application/json").post(Entity.entity(userCredentials,MediaType.APPLICATION_JSON),Usuario.class);
		
		//Si las credenciales son correctas se crea una sesion con el usuario pertinente
		if(result.getEmail() != null){
			HttpSession miSesion = request.getSession(true);
			miSesion.setAttribute("user",result);
			return true;
		}
		
		return false;
	}
	
	public List<Usuario> findAllUsers(){
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8010").path("/readAllClients");
		//Consumicion del microservicio
		List<Usuario> result=	webResource.request("application/json").accept("application/json").get(new GenericType<List<Usuario>>() {});

		return result;
	}
	
	public Usuario findUserById(String email){
		Usuario userSearch = new Usuario();
		userSearch.setEmail(email);
		
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8010").path("/getClient");
		//Consumicion del microservicio
		Usuario result = webResource.request("application/json").accept("application/json").post(Entity.entity(userSearch,MediaType.APPLICATION_JSON),Usuario.class);

		return result;
	}
	
	public Usuario findUserByEmail(HttpServletRequest request) {
		Usuario userSearch = new Usuario();
		userSearch.setEmail(request.getParameter("userToModify"));
		
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8010").path("/getClient");
		//Consumicion del microservicio
		Usuario result = webResource.request("application/json").accept("application/json").post(Entity.entity(userSearch,MediaType.APPLICATION_JSON),Usuario.class);

		return result;
	}
	
	public Usuario findUserByEmail(String email) {
		Usuario userSearch = new Usuario();
		userSearch.setEmail(email);
		
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8010").path("/getClient");
		//Consumicion del microservicio
		Usuario result = webResource.request("application/json").accept("application/json").post(Entity.entity(userSearch,MediaType.APPLICATION_JSON),Usuario.class);

		return result;
	}
	
	public boolean modifyUser(HttpServletRequest request){
		boolean result = true;
		String email = request.getParameter("userToModify");
		Usuario userChanges = findUserByEmail(email);
		
		//Se comprueban los campos a actualizar
		if(!request.getParameter("nameChange").equals("")) userChanges.setName(request.getParameter("nameChange"));
		if(!request.getParameter("firstsurnameChange").equals("")) userChanges.setFirstsurname(request.getParameter("firstsurnameChange"));
		if(!request.getParameter("secondsurnameChange").equals("")) userChanges.setSecondsurname(request.getParameter("secondsurnameChange"));
		if(!request.getParameter("ciudad").equals("")) userChanges.setCity(request.getParameter("ciudad"));
		
		//Si se cambia la contrasena, la antigua debe coincidir
		String passCifrada = request.getParameter("oldPasswordChange");
		if(passCifrada.equals(userChanges.getPassword()) && !request.getParameter("newPasswordChange").equals(""))
			userChanges.setPassword(request.getParameter("newPasswordChange"));
		else if (!request.getParameter("oldPasswordChange").equals(userChanges.getPassword()) && !request.getParameter("newPasswordChange").equals(""))
			result = false;
		
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8010").path("/modifyClient");
		//Consumicion del microservicio
		Result resultWebservice = webResource.request("application/json").accept("application/json").post(Entity.entity(userChanges,MediaType.APPLICATION_JSON),Result.class);
		
		//Si se ha podido updatear el usuario
		if(resultWebservice.isResult()){
			//Se actualiza el atributo en la pagina
			request.setAttribute("user", userChanges);
		}
		else result = false;
		
		return result;
	}
	
	public boolean removeUser(HttpServletRequest request) {
		//Se obtiene el atributo usuario
		String email = request.getParameter("userToModify");
		Usuario userToDelete = findUserByEmail(email);
		
		//Se actualiza el atributo en la pagina
		request.setAttribute("user", userToDelete);
		
		//obtiene la contraseņa del usuario
		String sessionPassword = userToDelete.getPassword();
		//obtiene la contraseņa introducida por el usuario
		String requestPassword=request.getParameter("passwordDelete");
		
		if(!sessionPassword.equals(requestPassword)) return false;
		//si las contrasenias coinciden se elimina la cuenta
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8010").path("/deleteClient");
		//Consumicion del microservicio
		Result resultWebservice = webResource.request("application/json").accept("application/json").post(Entity.entity(userToDelete,MediaType.APPLICATION_JSON),Result.class);
		
		if(resultWebservice.isResult()) {
			//Borrar productos asociados al usuario
			Producto prod = new Producto();
			prod.setEmailOwner(userToDelete.getEmail());
			//Preparacion para consumir microservicio
			Client client3 = ClientBuilder.newClient();
			WebTarget webResource3 = client3.target("http://localhost:8020").path("/removeByUser");
			//Consumicion del microservicio
			webResource3.request("application/json").accept("application/json").post(Entity.entity(prod,MediaType.APPLICATION_JSON));
			
			//Borrar mensajes asociados al usuario
			Chat chat = new Chat();
			chat.setSeller(userToDelete.getEmail());
			chat.setBuyer(userToDelete.getEmail());
			//Preparacion para consumir microservicio
			Client client4 = ClientBuilder.newClient();
			WebTarget webResource4 = client4.target("http://localhost:8030").path("/removeBySellerOrBuyer");
			//Consumicion del microservicio
			webResource4.request("application/json").accept("application/json").post(Entity.entity(chat,MediaType.APPLICATION_JSON));
			
			return true;
		}
		else return false;
	} 
}