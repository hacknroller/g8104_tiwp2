<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <title>Perfil | E-Shopper</title>
	    <link href="css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/font-awesome.min.css" rel="stylesheet">
	    <link href="css/prettyPhoto.css" rel="stylesheet">
	    <link href="css/price-range.css" rel="stylesheet">
	    <link href="css/animate.css" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet">
		<link href="css/responsive.css" rel="stylesheet">
	    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
	    <![endif]-->       
	    <link rel="shortcut icon" href="images/ico/favicon.ico">
	    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	</head><!--/head-->

	<body>
		<header id="header"><!--header-->
			<div class="header-middle"><!--header-middle-->
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<div class="logo pull-left">
								<a href="/G8104wallapopP2/homePage"><img src="images/home/logo.png" alt="" /></a>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="shop-menu pull-right">
								<ul class="nav navbar-nav">
								<li><a href="/G8104wallapopP2/myProductsPage"><i class="fa fa-star"></i> Mis productos</a></li>
								<li><a href="/G8104wallapopP2/myChatsPage"><i class="fa fa-comment-o"></i> Mis chats</a></li>
								<li><a href="/G8104wallapopP2/advancedSearch"><i class="fa fa-crosshairs"></i> Busqueda Avanzada</a></li>
								<li><a href="/G8104wallapopP2/logout" class="active"><i class="fa fa-lock"></i> Logout</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div><!--/header-middle-->
		
			<div class="header-bottom"><!--header-bottom-->
				<div class="container">
					<div class="row">
						<div class="col-sm-9">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div class="mainmenu pull-left">
								<ul class="nav navbar-nav collapse navbar-collapse">
									<li><a href="/G8104wallapopP2/homePage">Home</a></li>
	                                </li> 
									<li><a href="/G8104wallapopP2/aboutUsPage">Contacto</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="search_box pull-right">
								<form method="POST" action="/G8104wallapopP2/searchPage" class="searchform">
									<input type="text" maxlength="50" name="titleAndDescription" placeholder="Search" required/>
									<button type="submit" class="btn btn-default"><img src="images/home/searchicon.png"/></button>
								</form>	
							</div>
						</div>
					</div>
				</div>
			</div><!--/header-bottom-->
		</header><!--/header-->
	
	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Tus datos</h2>
						<jsp:useBean id="user" class="g8104.data.Usuario" scope="session" />
						<p><b>Nombre:</b><br><jsp:getProperty property="name" name="user"/></p>
						<br>
						<p><b>Primer apellido:</b><br><jsp:getProperty property="firstsurname" name="user"/></p>
						<br>
						<p><b>Segundo apellido:</b><br><jsp:getProperty property="secondsurname" name="user"/></p>
						<br>
						<p><b>Email:</b><br><jsp:getProperty property="email" name="user"/></p>
						<br>
						<p><b>Ciudad:</b><br><jsp:getProperty property="city" name="user"/></p>
					</div><!--/sign up form-->
				</div>
				
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Modifica tus datos</h2>
						<form method="POST" action="/G8104wallapopP2/modifyUser">
							<input type="text" maxlength="15" name="nameChange" placeholder="Nombre"/>
							<input type="text" maxlength="15" name="firstsurnameChange" placeholder="Primer apellido"/>
							<input type="text" maxlength="15" name="secondsurnameChange" placeholder="Segundo apellido"/>
							<input type="password" maxlength="15" name="oldPasswordChange" placeholder="Antiguo Password"/>
							<input type="password" maxlength="15" name="newPasswordChange" placeholder="Nuevo Password"/>
							<input type="text" maxlength="15" name="ciudad" placeholder="Ciudad"/>
							<button type="submit" class="btn btn-default">Editar</button>
						</form>
						<c:if test="${changeFailMessage != null}">
							<br>
							<p style="color:red;"><c:out value="${changeFailMessage}"/></p>
						</c:if>
					</div><!--/sign up form-->
				</div>

				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Elimina tu cuenta</h2>
						<form method="POST" action="/G8104wallapopP2/removeUser">
							<input type="password" maxlength="15" name="passwordDelete" placeholder="Password"/>
							<button type="submit" class="btn btn-default">Eliminar cuenta</button>
						</form>
						<c:if test="${removeFailMessage != null}">
							<br>
							<p style="color:red;"><c:out value="${removeFailMessage}"/></p>
						</c:if>
					</div><!--/sign up form-->
				</div>
				<br>
				<br>	
			</div>
			<br><br><br>
		</div>
	</section><!--/form-->

		<footer id="footer"><!--Footer-->
			<div class="footer-widget">
				<div class="container">
					<div class="row">
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Servicios</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Ayuda</a></li>
									<li><a href="">Contacto</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Comprar</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Telefonía</a></li>
									<li><a href="">Ropa</a></li>
									<li><a href="">Tecnología</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Política</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Terminos de uso</a></li>
									<li><a href="">Política de privacidad</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Sobre nosotros</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Infomración de la compañía</a></li>
									<li><a href="">Localización</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-3 col-sm-offset-1">
						</div>
						
					</div>
				</div>
			</div>
			
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<p class="pull-left">Copyright © 2013 WALLAPOP Inc. All rights reserved.</p>
					</div>
				</div>
			</div>
			
		</footer><!--/Footer-->
		
	    <script src="js/jquery.js"></script>
		<script src="js/price-range.js"></script>
	    <script src="js/jquery.scrollUp.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	    <script src="js/jquery.prettyPhoto.js"></script>
	    <script src="js/main.js"></script>
	</body>
</html>