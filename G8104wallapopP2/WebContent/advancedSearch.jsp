<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <title>Advanced Search | Wallapop</title>
	    <link href="css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/font-awesome.min.css" rel="stylesheet">
	    <link href="css/prettyPhoto.css" rel="stylesheet">
	    <link href="css/price-range.css" rel="stylesheet">
	    <link href="css/animate.css" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet">
		<link href="css/responsive.css" rel="stylesheet">
	    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
	    <![endif]-->       
	    <link rel="shortcut icon" href="images/ico/favicon.ico">
	    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	</head><!--/head-->

	<body>
		<header id="header"><!--header-->
			<div class="header-middle"><!--header-middle-->
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<div class="logo pull-left">
								<a href="/G8104wallapopP2/homePage"><img src="images/home/logo.png" alt="" /></a>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="shop-menu pull-right">
								<ul class="nav navbar-nav">
									<%if(session.getAttribute("user") == null){%>
										<li><a href="/G8104wallapopP2/loginPage" class="active"><i class="fa fa-lock"></i> Login</a></li>
									<%}else{%>
										<jsp:useBean id="user" class="g8104.data.Usuario" scope="session" />
										<li><a href="/G8104wallapopP2/accountPage"><i class="fa fa-user"></i> <jsp:getProperty property="name" name="user"/> <jsp:getProperty property="firstsurname" name="user"/></a></li>
										<li><a href="/G8104wallapopP2/myProductsPage"><i class="fa fa-star"></i> Mis productos</a></li>
										<li><a href="/G8104wallapopP2/myChatsPage"><i class="fa fa-comment-o"></i> Mis chats</a></li>
										<li><a href="/G8104wallapopP2/logout" class="active"><i class="fa fa-lock"></i> Logout</a></li>
									<%}%>	
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div><!--/header-middle-->
		
			<div class="header-bottom"><!--header-bottom-->
				<div class="container">
					<div class="row">
						<div class="col-sm-9">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div class="mainmenu pull-left">
								<ul class="nav navbar-nav collapse navbar-collapse">
									<li><a href="/G8104wallapopP2/homePage">Home</a></li>
									<li><a href="/G8104wallapopP2/aboutUsPage">Contacto</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="search_box pull-right">
								<form method="POST" action="/G8104wallapopP2/searchPage" class="searchform">
									<input type="text" maxlength="50" name="titleAndDescription" placeholder="Search" required/>
									<button type="submit" class="btn btn-default"><img src="images/home/searchicon.png"/></button>
								</form>	
							</div>
						</div>
					</div>
				</div>
			</div><!--/header-bottom-->
		</header><!--/header-->
		
		<section id="form"><!--form-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="signup-form"><!--sign up form-->
							<h2>Filtrar por campos</h2>
							<form method="GET" action="/G8104wallapopP2/advancedSearch" enctype="multipart/form-data">
								<input type="text" maxlength="15" name="title" placeholder="T&iacute;tulo"/>
								<textarea id="areadetexto" maxlength="500" name="description" placeholder="Descripci&oacute;n"></textarea>
								<select name="categoria">
									<option selected disabled hidden value="">Categoria</option>
									<option value="Electronica">Electronica</option>
									<option value="Moda y belleza">Moda y belleza</option>
									<option value="Ocio">Ocio</option>
									<option value="Vehiculos">Vehiculos</option>
									<option value="Otros">Otros</option>
								</select>
								<input type="text" maxlength="15" name="ciudad" placeholder="Ciudad"/>
								<input type="text" maxlength="15" name="vendedor" placeholder="Vendedor"/>
								<input type="hidden" name="go" value="1"/>
								<button type="submit" class="btn btn-default">Buscar</button>
							</form>
						</div><!--/sign up form-->
						<br>
					</div>
				</div>
			</div>
		</section><!--/form-->
		
			
		<section>
			<div class="container">
				<div class="row">
				<c:if test="${result == null && noProductsFoundMessage == null}">
					<br><br><br><br>
				</c:if>
				<c:if test="${result != null}">
					<h2 class="title text-center">Resultados de la Busqueda</h2>
					<c:if test="${firstProduct == null}">
						<c:set var="firstProduct" value="0"/>
					</c:if>
					<c:set var="lastProduct" value="${firstProduct+3}"/>
						
							
					<c:forEach items="${result}" begin="${firstProduct}" end="${lastProduct}" var="currentProduct" varStatus="ii">
						<div class="col-sm-3">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img src="<c:url value="http://localhost:8080/images/${currentProduct.getId()}.jpg"/>" alt="" />
										<h2>${currentProduct.getPrice()}€</h2>
										<p>${currentProduct.getTitle()}</p>
										<p>${requiredUsers.get(ii.index).getCity()}</p>
										<p>${requiredUsers.get(ii.index).getEmail()}</p>
										<form method="POST" action="/G8104wallapopP2/productDetailsPage">
											<input type="hidden" name="productToExplore" value="<c:url value="${currentProduct.getId()}"/>"/>
											<button type="submit" class="btn btn-default add-to-cart">Detalles</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</c:forEach>
					</c:if>
					<c:if test="${noProductsFoundMessage != null}">
						<br>
						<h2 class="title text-center">Resultados de la Busqueda</h2>
						<p style="color:gray;" class="text-center"><c:out value="${noProductsFoundMessage}"/></p>
					</c:if>
				</div>
				
				<div class="row">
				<c:if test="${numberOfPages != null}">
					<c:forEach var="i" begin="1" end="${numberOfPages}">
						<form method="GET" action="/G8104wallapopP2/advancedSearch" style="display: inline;">
							<input type="hidden" name="title" value="${title}"/>
							<input type="hidden" name="description" value="${description}"/>
							<input type="hidden" name="categoria" value="${categoria}"/>
							<input type="hidden" name="ciudad" value="${ciudad}"/>
							<input type="hidden" name="vendedor" value="${vendedor}"/>
							<input type="hidden" name="go" value="1"/>
							
							<input type="hidden" name="numberPageProduct" value="${i}"/>
							<button type="submit" class="btn btn-default add-to-cart" style="display:inline;">${i}</button>
						</form>
					</c:forEach>
				</c:if>	
				</div>
				
			</div>
		</section>
		
		
		
		<footer id="footer"><!--Footer-->
			<div class="footer-widget">
				<div class="container">
					<div class="row">
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Servicios</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Ayuda</a></li>
									<li><a href="">Contacto</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Comprar</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Telefonía</a></li>
									<li><a href="">Ropa</a></li>
									<li><a href="">Tecnología</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Política</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Terminos de uso</a></li>
									<li><a href="">Política de privacidad</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Sobre nosotros</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Infomración de la compañía</a></li>
									<li><a href="">Localización</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-3 col-sm-offset-1">
						</div>
						
					</div>
				</div>
			</div>
			
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<p class="pull-left">Copyright © 2013 WALLAPOP Inc. All rights reserved.</p>
					</div>
				</div>
			</div>
			
		</footer><!--/Footer-->
		
	    <script src="js/jquery.js"></script>
		<script src="js/price-range.js"></script>
	    <script src="js/jquery.scrollUp.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	    <script src="js/jquery.prettyPhoto.js"></script>
	    <script src="js/main.js"></script>
	</body>
</html>