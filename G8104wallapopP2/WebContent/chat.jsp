<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Product Details</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <title>Product Details | E-Shopper</title>
	    <link href="css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/font-awesome.min.css" rel="stylesheet">
	    <link href="css/prettyPhoto.css" rel="stylesheet">
	    <link href="css/price-range.css" rel="stylesheet">
	    <link href="css/animate.css" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet">
		<link href="css/responsive.css" rel="stylesheet">
	    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
	    <![endif]-->
	    <link rel="shortcut icon" href="images/ico/favicon.ico">
	    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	</head>
	<body>
		<c:set var="urlCor1" value="/G8104wallapopP2/contactUser?userLector="/>
		<c:set var="url" value="${urlCor1}${userProduct.getEmail()}"/>
		
		<header id="header"><!--header-->
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="/G8104wallapopP2/homePage"><img src="images/home/logo.png" alt="" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<%if(session.getAttribute("user") == null){%>
									<li><a href="/G8104wallapopP2/loginPage" class="active"><i class="fa fa-lock"></i> Login</a></li>
								<%}else{%>
									<jsp:useBean id="user" class="g8104.data.Usuario" scope="session" />
									<li><a href="/G8104wallapopP2/accountPage"><i class="fa fa-user"></i> <jsp:getProperty property="name" name="user"/> <jsp:getProperty property="firstsurname" name="user"/></a></li>
									<li><a href="/G8104wallapopP2/myProductsPage"><i class="fa fa-star"></i> Mis productos</a></li>
									<li><a href="/G8104wallapopP2/myChatsPage"><i class="fa fa-comment-o"></i> Mis chats</a></li>
									<li><a href="/G8104wallapopP2/advancedSearch"><i class="fa fa-crosshairs"></i> Busqueda Avanzada</a></li>
									<li><a href="/G8104wallapopP2/logout" class="active"><i class="fa fa-lock"></i> Logout</a></li>
								<%}%>	
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->

		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="/G8104wallapopP2/homePage">Home</a></li>
								<li class="dropdown"><a href="/G8104wallapopP2/aboutUsPage">Contacto</a>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<form method="POST" action="/G8104wallapopP2/searchPage" class="searchform">
								<input type="text" maxlength="50" name="titleAndDescription" placeholder="Search" required/>
								<button type="submit" class="btn btn-default"><img src="images/home/searchicon.png"/></button>
							</form>	
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->

	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 padding-right">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-comment"> ${productTitle}</span>
                </div>
                <div class="panel-body body-panel">
                    <ul class="chat">
	                    <c:if test="${messages.isEmpty()}">
	            			<p style="color:gray;" class="text-center">Todavia no hay mensajes</p>
	            		</c:if>
	                    <c:forEach items="${messages}" var="current">
	                        <li class="left clearfix" style="margin-left: 0px;">
	                            <div class="chat-body clearfix">
	                                <div class="header">
	                                    <strong class="primary-font">${current.getAuthor()}</strong>
	                                </div>
	                                <p>${current.getMessage()}</p>
	                            </div>
	                        </li>           
	                    </c:forEach>
                    </ul>
                </div>
                <div class="panel-footer clearfix">
                    <textarea class="form-control" rows="3" name="message" form="sendForm" required></textarea>
                    
                    <div class="signup-form"><!--sign up form-->
	                    <form method="POST" action="/G8104wallapopP2/sendMessage" style="display: inline;" id="sendForm">
	                    	<input type="hidden" name="seller" value="<c:url value="${seller}"/>"/>
			                <input type="hidden" name="buyer" value="<c:url value="${buyer}"/>"/>
			                <input type="hidden" name="product" value="<c:url value="${product}"/>"/>
			                <input type="hidden" name="author" value="<c:url value="${user.getEmail()}"/>"/>
			                <input type="hidden" name="productTitle" value="<c:url value="${productTitle}"/>"/>
	                        <button type="submit" class="btn btn-default" style="margin-top:10px; float:left; margin-right:10px;">Enviar</button>
	                    </form>
                    </div>
                    
                    <div class="signup-form"><!--sign up form-->
	                    <form method="POST" action="/G8104wallapopP2/chatPage" style="display: inline;">
	                    	<input type="hidden" name="seller" value="<c:url value="${seller}"/>"/>
			                <input type="hidden" name="buyer" value="<c:url value="${buyer}"/>"/>
			                <input type="hidden" name="product" value="<c:url value="${product}"/>"/>
			                <input type="hidden" name="productTitle" value="<c:url value="${productTitle}"/>"/>
	                        <button type="submit" class="btn btn-default" style="margin-top:10px;">Actualizar mensajes</button>
	                    </form>
                    </div>
                </div>
            </div>
				</div>
			</div>
		</div>
	</section>
	<br><br><br>
	<footer id="footer"><!--Footer-->


    <div class="footer-widget">
				<div class="container">
					<div class="row">
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Servicios</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Ayuda</a></li>
									<li><a href="">Contacto</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Comprar</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Telefon&iacute;a</a></li>
									<li><a href="">Ropa</a></li>
									<li><a href="">Tecnolog&iacute;a</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Pol&iacute;tica</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Terminos de uso</a></li>
									<li><a href="">Pol&iacute;tica de privacidad</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Sobre nosotros</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Informaci&oacute;n de la compa&ntilde;&iacute;a</a></li>
									<li><a href="">Localizaci&oacute;n</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-3 col-sm-offset-1">
						</div>

					</div>
				</div>
			</div>

		<div class="footer-bottom">
			<div class="container">
				<div class="row">
				<p class="pull-left">Copyright © 2013 WALLAPOP Inc. All rights reserved.</p>
				</div>
			</div>
		</div>

	</footer><!--/Footer-->



    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
    <script src="js/displayWindow.js"></script>
</body>
	</body>
</html>