<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <title>Home | Wallapop</title>
	    <link href="css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/font-awesome.min.css" rel="stylesheet">
	    <link href="css/prettyPhoto.css" rel="stylesheet">
	    <link href="css/price-range.css" rel="stylesheet">
	    <link href="css/animate.css" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet">
		<link href="css/responsive.css" rel="stylesheet">
	    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
	    <![endif]-->       
	    <link rel="shortcut icon" href="images/ico/favicon.ico">
	    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	</head><!--/head-->

	<body>
		<header id="header"><!--header-->
			<div class="header-middle"><!--header-middle-->
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<div class="logo pull-left">
								<a href="/G8104wallapopP2/homePage"><img src="images/home/logo.png" alt="" /></a>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="shop-menu pull-right">
								<ul class="nav navbar-nav">
									<%if(session.getAttribute("user") == null){%>
										<li><a href="/G8104wallapopP2/advancedSearch"><i class="fa fa-crosshairs"></i> Busqueda Avanzada</a></li>
										<li><a href="/G8104wallapopP2/loginPage" class="active"><i class="fa fa-lock"></i> Login</a></li>
									<%}else{%>
										<jsp:useBean id="user" class="g8104.data.Usuario" scope="session" />
										<li><a href="/G8104wallapopP2/accountPage"><i class="fa fa-user"></i> <jsp:getProperty property="name" name="user"/> <jsp:getProperty property="firstsurname" name="user"/></a></li>
										<li><a href="/G8104wallapopP2/myProductsPage"><i class="fa fa-star"></i> Mis productos</a></li>
										<li><a href="/G8104wallapopP2/myChatsPage"><i class="fa fa-comment-o"></i> Mis chats</a></li>
										<li><a href="/G8104wallapopP2/advancedSearch"><i class="fa fa-crosshairs"></i> Busqueda Avanzada</a></li>
										<li><a href="/G8104wallapopP2/logout" class="active"><i class="fa fa-lock"></i> Logout</a></li>
									<%}%>	
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div><!--/header-middle-->
		
			<div class="header-bottom"><!--header-bottom-->
				<div class="container">
					<div class="row">
						<div class="col-sm-9">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div class="mainmenu pull-left">
								<ul class="nav navbar-nav collapse navbar-collapse">
									<li><a href="/G8104wallapopP2/homePage">Home</a></li>
	                                </li> 
									<li><a href="/G8104wallapopP2/aboutUsPage">Contacto</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="search_box pull-right">
								<form method="POST" action="/G8104wallapopP2/searchPage" class="searchform">
									<input type="text" maxlength="50" name="titleAndDescription" placeholder="Search" required/>
									<button type="submit" class="btn btn-default"><img src="images/home/searchicon.png"/></button>
								</form>	
							</div>
						</div>
					</div>
				</div>
			</div><!--/header-bottom-->
		</header><!--/header-->
		
		<section id="slider"><!--slider-->
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div id="slider-carousel" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
								<li data-target="#slider-carousel" data-slide-to="1"></li>
								<li data-target="#slider-carousel" data-slide-to="2"></li>
							</ol>
							
							<div class="carousel-inner">
								<div class="item active">
									<div class="col-sm-6">
										<h1><span>Walla</span>Pop</h1>
										<h2>Las Mejores Ofertas</h2>
										<p>Si quieres articulos a precio de risa o deshacerte de aquello que no necesitas, estas en el sitio indicado. Miles de ofertas cada dia. Encuentra la tuya!</p>
									</div>
									<div class="col-sm-6">
										<img src="images/home/girl1.jpg" class="girl img-responsive" alt="" />
									</div>
								</div>
								<div class="item">
									<div class="col-sm-6">
										<h1><span>Walla</span>Pop</h1>
										<h2>Busca Todo Lo Que Necesites</h2>
										<p>Necesitas un articulo y quieres ahorrar a la vez que lo compras? Empieza a buscar entre los miles de articulos de segunda mano que se ofertan.</p>
									</div>
									<div class="col-sm-6">
										<img src="images/home/girl2.jpg" class="girl img-responsive" alt="" />
									</div>
								</div>
								
								<div class="item">
									<div class="col-sm-6">
										<h1><span>Walla</span>Pop</h1>
										<h2>Vende Todo Lo Que No Utilices</h2>
										<p>Gana dinero vendiendo aquello que no usas y encuentra oportunidades cerca de ti. Es la oportunidad perfecta para liberar espacio y a la vez ganar algo de dinero.</p>
									</div>
									<div class="col-sm-6">
										<img src="images/home/girl3.jpg" class="girl img-responsive" alt="" />
									</div>
								</div>
								
							</div>
							
							<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							</a>
							<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</section><!--/slider-->
		
		<section>
			<div class="container">
				<div class="row">
				<h2 class="title text-center">Productos Recientes</h2>
						<div class="features_items"><!--features_items-->
								<c:forEach items="${currentProducts}" begin="0" end="3" var="currentProduct" varStatus="ii">
									<div class="col-sm-3">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<jsp:useBean id="now" class="java.util.Date" />
													<img src="<c:url value="http://localhost:8080/images/${currentProduct.getId()}.jpg?dummy=${now}"/>" alt="" />
													<h2>${currentProduct.getPrice()}€</h2>
													<p>${currentProduct.getTitle()}</p>
													<p>${currentUsers.get(ii.index).getCity()}</p>
													<p>${currentUsers.get(ii.index).getName()}</p>
												</div>
												
												<div class="product-overlay">
													<div class="overlay-content">
														<h2>${currentProduct.getTitle()}</h2>
														<h2>${currentProduct.getPrice()}€</h2>
														<p>${currentUsers.get(ii.index).getCity()}</p>
														<form method="POST" action="/G8104wallapopP2/productDetailsPage">
															<input type="hidden" name="productToExplore" value="<c:url value="${currentProduct.getId()}"/>"/>
															<button type="submit" class="btn btn-default add-to-cart">Detalles</button>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>
								</c:forEach>
								<c:if test="${currentProducts.isEmpty()}">
									<p style="color:gray;" class="text-center">No hay productos registrados en el sistema todavia</p>
								</c:if>
								
						</div><!--features_items-->
				</div>
			</div>
		</section>
		
		<footer id="footer"><!--Footer-->
			<div class="footer-widget">
				<div class="container">
					<div class="row">
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Servicios</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Ayuda</a></li>
									<li><a href="">Contacto</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Comprar</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Telefonía</a></li>
									<li><a href="">Ropa</a></li>
									<li><a href="">Tecnología</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Política</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Terminos de uso</a></li>
									<li><a href="">Política de privacidad</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Sobre nosotros</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Infomración de la compañía</a></li>
									<li><a href="">Localización</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-3 col-sm-offset-1">
						</div>
						
					</div>
				</div>
			</div>
			
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<p class="pull-left">Copyright © 2013 WALLAPOP Inc. All rights reserved.</p>
					</div>
				</div>
			</div>
			
		</footer><!--/Footer-->
		
	    <script src="js/jquery.js"></script>
		<script src="js/price-range.js"></script>
	    <script src="js/jquery.scrollUp.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	    <script src="js/jquery.prettyPhoto.js"></script>
	    <script src="js/main.js"></script>
	</body>
</html>