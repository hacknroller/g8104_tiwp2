<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <title>Login | E-Shopper</title>
	    <link href="css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/font-awesome.min.css" rel="stylesheet">
	    <link href="css/prettyPhoto.css" rel="stylesheet">
	    <link href="css/price-range.css" rel="stylesheet">
	    <link href="css/animate.css" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet">
		<link href="css/responsive.css" rel="stylesheet">
	    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
	    <![endif]-->       
	    <link rel="shortcut icon" href="images/ico/favicon.ico">
	    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	</head><!--/head-->

	<body>
		<header id="header"><!--header-->
			<div class="header-middle"><!--header-middle-->
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<div class="logo pull-left">
								<a href="/G8104wallapopP2/homePage"><img src="images/home/logo.png" alt="" /></a>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="shop-menu pull-right">
								<ul class="nav navbar-nav">
									<li><a href="/G8104wallapopP2/advancedSearch"><i class="fa fa-crosshairs"></i> Busqueda Avanzada</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div><!--/header-middle-->
		
			<div class="header-bottom"><!--header-bottom-->
				<div class="container">
					<div class="row">
						<div class="col-sm-9">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div class="mainmenu pull-left">
								<ul class="nav navbar-nav collapse navbar-collapse">
									<li><a href="/G8104wallapopP2/homePage">Home</a></li>
									<li><a href="/G8104wallapopP2/aboutUsPage">Contacto</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="search_box pull-right">
								<form method="POST" action="/G8104wallapopP2/searchPage" class="searchform">
									<input type="text" maxlength="50" name="titleAndDescription" placeholder="Search" required/>
									<button type="submit" class="btn btn-default"><img src="images/home/searchicon.png"/></button>
								</form>	
							</div>
						</div>
					</div>
				</div>
			</div><!--/header-bottom-->
		</header><!--/header-->
		
		<section id="form"><!--form-->
			<br>
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-1">
						<div class="login-form"><!--login form-->
							<h2>Login</h2>
							<form method="POST" action="/G8104wallapopP2/login">
								<input type="email" maxlength="25" name="email" placeholder="Email" required/>
								<input type="password" maxlength="15" name="password" placeholder="Password" required/>								
								<button type="submit" class="btn btn-default">Login</button>
							</form>
							<c:if test="${loginFail != null}">
								<br>
								<p style="color:red;"><c:out value="${loginFail}"/></p>
							</c:if>
						</div><!--/login form-->
					</div>
					
					<div class="col-sm-4 col-sm-offset-1">
						<div class="signup-form"><!--sign up form-->
							<h2>Nuevo usuario</h2>
							<form method="POST" action="/G8104wallapopP2/signup">
								<input type="text" maxlength="15" name="name" placeholder="Nombre" required/>
								<input type="text" maxlength="15" name="firstsurname" placeholder="Primer apellido" required/>
								<input type="text" maxlength="15" name="secondsurname" placeholder="Segundo apellido" required/>
								<input type="email" maxlength="25" name="email" placeholder="Email" required/>
								<input type="text" maxlength="15" name="city" placeholder="Ciudad" required/>
								<input type="password" maxlength="15" name="password" placeholder="Password" required/>
								<input type="password" maxlength="15" name="confirmpsw" placeholder="Confirme su Password" required/>
								<button type="submit" class="btn btn-default">Signup</button>
							</form>
							<c:if test="${signupFail != null}">
								<br>
								<p style="color:red;"><c:out value="${signupFail}"/></p>
							</c:if>
							<c:if test="${signupFail == null}">
								<br>
								<br>
							</c:if>						
						</div><!--/sign up form-->
					</div>
				</div>
			</div>
		</section><!--/form-->
	
		<footer id="footer"><!--Footer-->
			<div class="footer-widget">
				<div class="container">
					<div class="row">
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Servicios</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Ayuda</a></li>
									<li><a href="">Contacto</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Comprar</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Telefonía</a></li>
									<li><a href="">Ropa</a></li>
									<li><a href="">Tecnología</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Política</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Terminos de uso</a></li>
									<li><a href="">Política de privacidad</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Sobre nosotros</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Infomración de la compañía</a></li>
									<li><a href="">Localización</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-3 col-sm-offset-1">
						</div>
					</div>
				</div>
			</div>
			
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<p class="pull-left">Copyright © 2013 WALLAPOP Inc. All rights reserved.</p>
					</div>
				</div>
			</div>
		</footer><!--/Footer-->
		
	    <script src="js/jquery.js"></script>
		<script src="js/price-range.js"></script>
	    <script src="js/jquery.scrollUp.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	    <script src="js/jquery.prettyPhoto.js"></script>
	    <script src="js/main.js"></script>
	</body>
</html>