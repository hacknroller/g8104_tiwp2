package g8104.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import g8104.data.Producto;
import g8104.data.Usuario;
import g8104.microservices.CatalogMicroserviceProxy;
import g8104.microservices.ClientMicroserviceProxy;

public class AdvancedSearchPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Si no esta logueado se redirecciona a login
		HttpSession session = request.getSession(false);
		if (session == null) return new HomePage().handleRequest(request, response);
		else if(session != null) 
			if(session.getAttribute("user") == null) return new LoginPage().handleRequest(request, response);
		
		//Busqueda avanzada
		if(request.getParameter("go") != null){
			List<Producto> result = new CatalogMicroserviceProxy().advancedSearch(request);
			
			//Si hay productos encontrados
			if(!result.isEmpty()){
				//Usuarios asociados a los productos
				List<Usuario> requiredUsers = new ArrayList<Usuario>();
				
				//Productos filtrados por ciudad
				List<Producto> resultWithCity = new ArrayList<Producto>();
				
				//Si se ha buscado por ciudad tambien
				if(!request.getParameter("ciudad").equals("")){
					for(Producto current: result){
						Usuario user = new ClientMicroserviceProxy().findUserById(current.getEmailOwner());
						
						//Si el usuario tiene la ciudad solicitada
						if(user.getCity().equalsIgnoreCase(request.getParameter("ciudad"))){
							resultWithCity.add(current);
							requiredUsers.add(user);
						}
					}
				}
				else{
					for(Producto current: result){
						requiredUsers.add(new ClientMicroserviceProxy().findUserById(current.getEmailOwner()));
					}
				}
				
				//Si no hay productos porque no hay usuarios de esa ciudad
				if(!request.getParameter("ciudad").equals("") && resultWithCity.isEmpty()){
					request.setAttribute("noProductsFoundMessage", "No se ha encontrado ningun resultado");
				}
				else{
					//Numero de paginas de productos producidas
					int numberOfPages;
					if(!resultWithCity.isEmpty()){
						request.setAttribute("result", resultWithCity);
						numberOfPages = (int) (Math.ceil(resultWithCity.size()/4.0));
					}
					else{
						request.setAttribute("result", result);
						numberOfPages = (int) (Math.ceil(result.size()/4.0));
					}
					request.setAttribute("requiredUsers", requiredUsers);
					request.setAttribute("numberOfPages", numberOfPages);
					
					//Seleccionar pagina de productos a mostrar, y cual es el primer producto de esa pagina
					Integer firstProduct;
					String numberPageProductStr = request.getParameter("numberPageProduct");
					if(numberPageProductStr == null || numberPageProductStr.length() > 10) firstProduct = 0;
					else{
						if(Integer.parseInt(numberPageProductStr)<1 || Integer.parseInt(numberPageProductStr)>numberOfPages) firstProduct=0;
						else firstProduct = (Integer.parseInt(numberPageProductStr)-1)*4;
					}
					
					request.setAttribute("firstProduct", firstProduct);
					request.setAttribute("title", request.getParameter("title"));
					request.setAttribute("description", request.getParameter("description"));
					request.setAttribute("categoria", request.getParameter("categoria"));
					request.setAttribute("ciudad", request.getParameter("ciudad"));
					request.setAttribute("vendedor", request.getParameter("vendedor"));
				}
			}
			else request.setAttribute("noProductsFoundMessage", "No se ha encontrado ningun resultado");
		}
			
		return "advancedSearch.jsp";
	}
}