package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.microservices.CatalogMicroserviceProxy;

public class CreateProduct implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(new CatalogMicroserviceProxy().createProduct(request))
			request.setAttribute("createProductSuccess", "Su producto se ha registrado con exito!!");
		else
			request.setAttribute("createProductFail", "Ocurrio algun error durante la subida del producto. Intentelo de nuevo.");
		
		return new MyProductsPage().handleRequest(request, response);
	}
}