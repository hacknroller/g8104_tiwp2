package g8104.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.data.Producto;
import g8104.data.Usuario;
import g8104.microservices.CatalogMicroserviceProxy;
import g8104.microservices.ClientMicroserviceProxy;

public class HomePage implements Action {
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Lista de los 6 productos mas recientes y sus autores asociados
		List<Producto> currentProducts = new CatalogMicroserviceProxy().find6MostRecentProduct();
		request.setAttribute("currentProducts", currentProducts);
		
		List<Usuario> currentUsers = new ArrayList<Usuario>();
		for(Producto current: currentProducts){
			currentUsers.add(new ClientMicroserviceProxy().findUserById(current.getEmailOwner()));
		}
		request.setAttribute("currentUsers", currentUsers);
		
		return "index.jsp";
	}
}