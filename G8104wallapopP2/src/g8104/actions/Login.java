package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.microservices.ClientMicroserviceProxy;

public class Login implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba los datos de login, si son correctos se redirige a home
		if(new ClientMicroserviceProxy().loginUser(request)) return new HomePage().handleRequest(request, response);
		else{
			request.setAttribute("loginFail", "Las credenciales introducidas son incorrectas. IntÚntelo de nuevo.");
			return "login.jsp";
		}
	}
}