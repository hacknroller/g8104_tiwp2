package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginPage implements Action {
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Si esta logueado se redirige a home
		HttpSession session = request.getSession(false);
		if(session != null) if(session.getAttribute("user") != null)  return new HomePage().handleRequest(request, response);
		
		return "login.jsp";
	}
}