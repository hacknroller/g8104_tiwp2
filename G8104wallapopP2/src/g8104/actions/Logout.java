package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Logout implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Si tenia una sesion se desloguea
		HttpSession session = request.getSession(false);
		if(session != null){
		    session.invalidate();
		}
		
		return new HomePage().handleRequest(request, response);
	}
}