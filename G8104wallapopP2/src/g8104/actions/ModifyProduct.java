package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.data.Producto;
import g8104.microservices.CatalogMicroserviceProxy;

public class ModifyProduct implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Producto product = new CatalogMicroserviceProxy().modifyProduct(request);
		if(product != null){
			request.setAttribute("product", product);
			return "modifyProduct.jsp";
		}
		else
			return new HomePage().handleRequest(request, response);
	}
}