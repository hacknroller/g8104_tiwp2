package g8104.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.data.Chat;
import g8104.data.Producto;
import g8104.microservices.CatalogMicroserviceProxy;
import g8104.microservices.ChatMicroserviceProxy;

public class MyChatsPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Obtener la lista de Chats
		List<Chat> myChats = new ChatMicroserviceProxy().getChats(request);
		request.setAttribute("myChats", myChats);
		
		//Obtener info de productos asociados (solo para vista)
		List<Producto> products = new ArrayList<Producto>();
		for (Chat current : myChats){
			products.add(new CatalogMicroserviceProxy().findProductById(current.getProduct()));
		}
		request.setAttribute("products", products);
		
		return "mychats.jsp";
	}
}