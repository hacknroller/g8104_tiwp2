package g8104.actions;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import g8104.data.Producto;
import g8104.microservices.CatalogMicroserviceProxy;

public class MyProductsPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Si no esta logueado se redirige a home
		HttpSession session = request.getSession(false);
		if (session == null) return new HomePage().handleRequest(request, response);
		else if(session != null) 
			if(session.getAttribute("user") == null) return new HomePage().handleRequest(request, response);
		
		//Obtener productos del usuario
		List<Producto> productsOfUser = new CatalogMicroserviceProxy().findProductsByUser(request);
		if(!productsOfUser.isEmpty()){
			request.setAttribute("productsOfUser", productsOfUser);
			
			//Numero de paginas de productos producidas
			int numberOfPages = (int) (Math.ceil(productsOfUser.size()/4.0));
			request.setAttribute("numberOfPages", numberOfPages);
			
			//Seleccionar pagina de productos a mostrar, y cual es el primer producto de esa pagina
			Integer firstProduct;
			String numberPageProductStr = request.getParameter("numberPageProduct");
			if(numberPageProductStr == null || numberPageProductStr.length() > 10) firstProduct = 0;
			else{
				if(Integer.parseInt(numberPageProductStr)<1 || Integer.parseInt(numberPageProductStr)>numberOfPages) firstProduct=0;
				else firstProduct = (Integer.parseInt(numberPageProductStr)-1)*4;
			}
			
			request.setAttribute("firstProduct", firstProduct);
		}
		else request.setAttribute("noProductsYetMessage", "Todavia no has subido ningun articulo");
	
		return "myProducts.jsp";
	}
}