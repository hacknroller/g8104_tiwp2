package g8104.actions;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.data.Producto;
import g8104.data.Usuario;
import g8104.microservices.CatalogMicroserviceProxy;
import g8104.microservices.ClientMicroserviceProxy;

public class ProductDetailsPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se obtiene el producto a mostrar y el ususario asociado
		Producto product = new CatalogMicroserviceProxy().findProductById(request);
		request.setAttribute("product", product);
		
		Usuario userProduct = new ClientMicroserviceProxy().findUserById(product.getEmailOwner());
		request.setAttribute("userProduct", userProduct);
		
		return "ProductDetails.jsp";
	}
}