package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.microservices.CatalogMicroserviceProxy;

public class RemoveProduct implements Action {
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(new CatalogMicroserviceProxy().removeProduct(request)) return new MyProductsPage().handleRequest(request, response);
		else request.setAttribute("removeFailMessage", "No ha sido posible eliminar el producto");
		
		return new MyProductsPage().handleRequest(request, response);
	}
}