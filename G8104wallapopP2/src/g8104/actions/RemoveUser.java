package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.microservices.ClientMicroserviceProxy;


public class RemoveUser implements Action
{
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(new ClientMicroserviceProxy().removeUser(request)) return new HomePage().handleRequest(request, response);
		else request.setAttribute("removeFailMessage", "La password introducida es incorrecta");
		
		return "account.jsp";
	}
}