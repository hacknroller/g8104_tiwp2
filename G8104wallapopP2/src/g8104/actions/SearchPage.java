package g8104.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import g8104.data.Producto;
import g8104.data.Usuario;
import g8104.microservices.CatalogMicroserviceProxy;
import g8104.microservices.ClientMicroserviceProxy;

public class SearchPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Si no esta logueado se redirecciona a login
		HttpSession session = request.getSession(false);
		if (session == null) return new HomePage().handleRequest(request, response);
		else if(session != null) 
			if(session.getAttribute("user") == null) return new LoginPage().handleRequest(request, response);
		
		//Busqueda por titulo o descripcion
		List<Producto> requiredProducts = new CatalogMicroserviceProxy().findProductsByTitleAndDescription(request);		
		if(!requiredProducts.isEmpty()){
			request.setAttribute("requiredProducts", requiredProducts);
			
			//Numero de paginas de productos producidas
			int numberOfPages = (int) (Math.ceil(requiredProducts.size()/4.0));
			request.setAttribute("numberOfPages", numberOfPages);
			
			//Seleccionar pagina de productos a mostrar, y cual es el primer producto de esa pagina
			Integer firstProduct;
			String numberPageProductStr = request.getParameter("numberPageProduct");
			if(numberPageProductStr == null || numberPageProductStr.length() > 10) firstProduct = 0;
			else{
				if(Integer.parseInt(numberPageProductStr)<1 || Integer.parseInt(numberPageProductStr)>numberOfPages) firstProduct=0;
				else firstProduct = (Integer.parseInt(numberPageProductStr)-1)*4;
			}
			
			request.setAttribute("firstProduct", firstProduct);
			request.setAttribute("titleAndDescription", request.getParameter("titleAndDescription"));
			
			//Usuarios asociados a los productos
			List<Usuario> requiredUsers = new ArrayList<Usuario>();
			for(Producto current: requiredProducts){
				requiredUsers.add(new ClientMicroserviceProxy().findUserById(current.getEmailOwner()));
			}
			request.setAttribute("requiredUsers", requiredUsers);
		}
		else request.setAttribute("noProductsFoundMessage", "No se ha encontrado ningun resultado");
		
		return "search.jsp";
	}
}