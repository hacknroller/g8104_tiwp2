package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.microservices.ChatMicroserviceProxy;

public class SendMessage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(new ChatMicroserviceProxy().writeMessage(request)) return new ChatPage().handleRequest(request, response);
		else return new ChatPage().handleRequest(request, response);
	}
}