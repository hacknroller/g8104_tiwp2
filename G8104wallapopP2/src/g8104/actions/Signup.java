package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.microservices.ClientMicroserviceProxy;

public class Signup implements Action {
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Si se consigue registrar, se redirige al indice
		if(new ClientMicroserviceProxy().signupUser(request)) return new HomePage().handleRequest(request, response);

		//Si no, se redirige otra vez a la pagina de login
		else{
			request.setAttribute("signupFail", "Ya existe un usuario registrado con el email indicado o las contrasenas no coinciden.");
			return "login.jsp";
		}
	}
}