package g8104.business.image;

import java.io.IOException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

public interface ImageBusiness {
	/**
	 * Guarda una imagen de un usuario en local en la maquina del servidor
	 * 
	 * @param request - Peticion asociada
	 * @throws IOException
	 * @throws ServletException
	 * @throws NamingException 
	 */
	public String uploadImage(HttpServletRequest request, String newID) throws IOException, ServletException, NamingException;
}
