package g8104.data;

import java.io.Serializable;

public class Owner implements Serializable{
	private static final long serialVersionUID = 1L;
	
	String emailOwner;
	
	public Owner(){}

	public String getEmailOwner() {
		return emailOwner;
	}

	public void setEmailOwner(String emailOwner) {
		this.emailOwner = emailOwner;
	}
}
