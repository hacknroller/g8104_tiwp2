package g8104.microservices;

import java.io.IOException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import g8104.data.Chat;
import g8104.data.IdProducto;
import g8104.data.Owner;
import g8104.data.Producto;
import g8104.data.Result;
import g8104.data.Search;
import g8104.data.Usuario;

public class CatalogMicroserviceProxy {
	
	public boolean createProduct(HttpServletRequest request) {
		Producto updateProduct = new Producto();
		Usuario user = (Usuario) request.getSession().getAttribute("user");
		
		//Rellenar updateProduct
		updateProduct.setTitle(request.getParameter("title"));
		updateProduct.setPrice(request.getParameter("price"));
		updateProduct.setDescription(request.getParameter("description"));
		updateProduct.setCategory(request.getParameter("categoria"));
		updateProduct.setState("Disponible");
		updateProduct.setEmailOwner(user.getEmail());
		
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8020").path("/createProduct");
		//Consumicion del microservicio
		Producto result = webResource.request("application/json").accept("application/json").post(Entity.entity(updateProduct,MediaType.APPLICATION_JSON),Producto.class);
		
		//Se hace upload de la imagen
		try {
			new g8104.business.image.ImageBusinessDefault().uploadImage(request, result.getId());
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (ServletException e) {
			e.printStackTrace();
			return false;
		} catch (NamingException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public List<Producto> find6MostRecentProduct(){
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8020").path("/readMostRecenProducts");
		//Consumicion del microservicio
		List<Producto> result=	webResource.request("application/json").accept("application/json").get(new GenericType<List<Producto>>() {});
				
		return result;
	}
	
	public List<Producto> findProductsByUser(HttpServletRequest request){
		Usuario user = (Usuario) request.getSession(false).getAttribute("user");
		Owner owner = new Owner();
		owner.setEmailOwner(user.getEmail());
		
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8020").path("/readByEmailOwner");
		//Consumicion del microservicio
		List<Producto> result = webResource.request("application/json").accept("application/json").post(Entity.entity(owner,MediaType.APPLICATION_JSON),new GenericType<List<Producto>>() {});
		
		return result;
	}
	
	public Producto findProductById(HttpServletRequest request) {
		IdProducto idProducto = new IdProducto();
		idProducto.setId(request.getParameter("productToExplore"));
		
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8020").path("/readById");
		//Consumicion del microservicio
		Producto result = webResource.request("application/json").accept("application/json").post(Entity.entity(idProducto,MediaType.APPLICATION_JSON),Producto.class);
		
		return result;
	}
	
	public Producto findProductById(String id) {
		IdProducto idProducto = new IdProducto();
		idProducto.setId(id);
		
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8020").path("/readById");
		//Consumicion del microservicio
		Producto result = webResource.request("application/json").accept("application/json").post(Entity.entity(idProducto,MediaType.APPLICATION_JSON),Producto.class);
		
		return result;
	}
	
	public List<Producto> findProductsByTitleAndDescription(HttpServletRequest request) {
		Search search = new Search();
		search.setTitle(request.getParameter("titleAndDescription"));
		search.setDescription(request.getParameter("titleAndDescription"));
		
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8020").path("/readByTitleAndDescription");
		//Consumicion del microservicio
		List<Producto> result = webResource.request("application/json").accept("application/json").post(Entity.entity(search,MediaType.APPLICATION_JSON),new GenericType<List<Producto>>() {});
		
		return result;
	}
	
	public List<Producto> advancedSearch(HttpServletRequest request) {
		Search search = new Search();
		search.setTitle("");
		search.setDescription("");
		search.setCategory("");
		search.setUser("");
		
		if(!request.getParameter("title").equals("")) search.setTitle(request.getParameter("title"));
		if(!request.getParameter("description").equals("")) search.setDescription(request.getParameter("description"));
		if(request.getParameter("categoria") != null) search.setCategory(request.getParameter("categoria"));
		if(!request.getParameter("vendedor").equals("")) search.setUser(request.getParameter("vendedor"));
		
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8020").path("/advancedSearchProduct");
		//Consumicion del microservicio
		List<Producto> result = webResource.request("application/json").accept("application/json").post(Entity.entity(search,MediaType.APPLICATION_JSON),new GenericType<List<Producto>>() {});
		
		return result;
	}
	
	public Producto modifyProduct(HttpServletRequest request) {
		//Se obtiene el producto a modificar
		IdProducto idProducto = new IdProducto();
		idProducto.setId(request.getParameter("productToModify"));
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8020").path("/readById");
		//Consumicion del microservicio
		Producto productChanges = webResource.request("application/json").accept("application/json").post(Entity.entity(idProducto,MediaType.APPLICATION_JSON),Producto.class);
		
		if(!request.getParameter("titleChange").equals("")) productChanges.setTitle(request.getParameter("titleChange"));
		if(request.getParameter("estadoChange") != null) productChanges.setState(request.getParameter("estadoChange"));
		if(request.getParameter("categoriaChange") != null) productChanges.setCategory(request.getParameter("categoriaChange"));
		if(!request.getParameter("descriptionChange").equals("")) productChanges.setDescription(request.getParameter("descriptionChange"));
		if(!request.getParameter("priceChange").equals("")) productChanges.setPrice(request.getParameter("priceChange"));
		
		//Update image
		try {
			if(!request.getPart("file").getSubmittedFileName().equals("")){
				try {
					new g8104.business.image.ImageBusinessDefault().uploadImage(request, productChanges.getId());
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				} catch (ServletException e) {
					e.printStackTrace();
					return null;
				} catch (NamingException e) {
					e.printStackTrace();
					return null;
				}			
			}
		} catch (IOException | ServletException e) {
			e.printStackTrace();
		}
		
		//Modificar el producto
		//Preparacion para consumir microservicio
		Client client2 = ClientBuilder.newClient();
		WebTarget webResource2 = client2.target("http://localhost:8020").path("/modifyProduct");
		//Consumicion del microservicio
		Result result = webResource2.request("application/json").accept("application/json").post(Entity.entity(productChanges,MediaType.APPLICATION_JSON),Result.class);
		
		if(result.isResult()) return productChanges;
		else return null;
	}
	
	public boolean removeProduct(HttpServletRequest request) {
		//Se rellena el producto a modificar (solo interesa el id)
		Producto deleteProduct = new Producto();
		deleteProduct.setId(request.getParameter("productToExplore"));

		//Preparacion para consumir microservicio
		Client client2 = ClientBuilder.newClient();
		WebTarget webResource2 = client2.target("http://localhost:8020").path("/deleteProduct");
		//Consumicion del microservicio
		Result result = webResource2.request("application/json").accept("application/json").post(Entity.entity(deleteProduct,MediaType.APPLICATION_JSON),Result.class);

		//Si se completo la operacion con exito
		if(result.isResult()) {
			Chat chat = new Chat();
			chat.setProduct(deleteProduct.getId());
			
			//Preparacion para consumir microservicio
			Client client3 = ClientBuilder.newClient();
			WebTarget webResource3 = client3.target("http://localhost:8030").path("/removeByProduct");
			//Consumicion del microservicio
			webResource3.request("application/json").accept("application/json").post(Entity.entity(chat,MediaType.APPLICATION_JSON));

			return true;
		}
		else return false;
	}
}