package g8104.microservices;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import g8104.data.Chat;
import g8104.data.ChatMessage;
import g8104.data.Producto;
import g8104.data.Result;
import g8104.data.Usuario;

public class ChatMicroserviceProxy {
	
	public List<Chat> getChats(HttpServletRequest request){
		Usuario user = (Usuario) request.getSession().getAttribute("user");
		
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8030").path("/readChats");
		//Consumicion del microservicio
		List<Chat> result = webResource.request("application/json").accept("application/json").post(Entity.entity(user,MediaType.APPLICATION_JSON),new GenericType<List<Chat>>() {});

		return result;
	}
	
	public List<ChatMessage> getMessages(HttpServletRequest request){
		Chat chat = new Chat();
		chat.setSeller(request.getParameter("seller"));
		chat.setBuyer(request.getParameter("buyer"));
		chat.setProduct(request.getParameter("product"));
		
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8030").path("/readMessages");
		//Consumicion del microservicio
		List<ChatMessage> result = webResource.request("application/json").accept("application/json").post(Entity.entity(chat,MediaType.APPLICATION_JSON),new GenericType<List<ChatMessage>>() {});

		return result;
	}
	
	public boolean writeMessage(HttpServletRequest request){
		ChatMessage message = new ChatMessage();
		message.setSeller(request.getParameter("seller"));
		message.setBuyer(request.getParameter("buyer"));
		message.setProduct(request.getParameter("product"));
		message.setAuthor(request.getParameter("author"));
		message.setMessage(request.getParameter("message"));
		
		//Preparacion para consumir microservicio
		Client client = ClientBuilder.newClient();
		WebTarget webResource = client.target("http://localhost:8030").path("/createMessage");
		//Consumicion del microservicio
		Result result = webResource.request("application/json").accept("application/json").post(Entity.entity(message,MediaType.APPLICATION_JSON),Result.class);

		if(result.isResult()) return true;
		else return false;
	}
}